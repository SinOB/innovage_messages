=== Innovage Messages ===
Contributors: SinOB
Tags: buddypress
Requires at least: WP 4.0, BuddyPress 2.0.3
Tested up to: WP 4.0, BuddyPress 2.0.3
Stable tag: 1.0
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Allows members to send hardcoded 'Kudos' messages to other site members.

== Description ==

Allows members to send hardcoded 'Kudos' messages to other site members. Hides
site wide and group activity streams. Limits profile activity to 
Personal and Mentions(where mentions has been renamed to Kudos)

There are currently no admin options.


== Installation ==

1. Download
2. Upload to your '/wp-contents/plugins/' directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.
