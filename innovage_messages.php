<?php
/*
  Plugin Name: Innovage Messages
  Plugin URI: https://bitbucket.org/SinOB/innovage_messages
  Description: Allows members to send hardcoded 'Kudos' messages to other site members.
  Author: Sinead O'Brien
  Author URI: https://bitbucket.org/SinOB
  Version: 1.0
  Requires at least: WP 4.0, BuddyPress 2.0.3
  Tested up to: WP 4.0.1, BuddyPress 2.0.3
  License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/gpl.html
 */

add_action('wp', 'innovage_messages_activity_redirect', 3);

/** /
 * Redirect anyone trying to get to the buddypress standard page to the new custom
 * message page
 *
 * @global type $wp
 * @global type $bp
 * @return type
 */
function innovage_messages_activity_redirect() {
    global $bp;
    // logged out users should already be covered by innovage_lockdown_redirect()

    if (is_user_logged_in()) {
        if (bp_current_component() == 'activity' && !bp_is_user()) {
            $recipient = isset($_GET['r']) ? esc_textarea($_GET['r']) : '';
            bp_core_redirect(get_option('siteurl') . '/message?r=' . $recipient);
            return;
        }
    }
}

add_shortcode('show_innovage_add_message_form', 'innovage_messages_view_add_page');

/** /
 * Add content to the message page using the shortcode [innovage_messages_view_add_page]
 *
 * @global type $bp
 * @param type $atts
 * @param type $content
 * @return string
 */
function innovage_messages_view_add_page($atts, $content = '') {
    global $bp;

    // If user isn't logged in hide the page
    if (!is_user_logged_in()) {
        return "Please log in to view this content.";
    }
    if (!isset($_GET["r"])) {
        return "Error. Invalid or missing recipient!";
    }
    $recipient_slug = $_GET["r"];

    // test that we have a valid username as the recipient
    $recipient = get_user_by('slug', $recipient_slug);
    if (!isset($recipient) || is_wp_error($recipient) || $recipient == false) {
        innovage_messsages_display_error("Invalid or missing recipient!");
        return;
    }

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (!isset($_POST['whats-new-select']) || $_POST['whats-new-select'] == '') {
            innovage_messsages_display_error("Invalid kudos message!");
            return;
        }
        $result = innovage_message_add_activity($recipient);
        if (!$result) {
            innovage_messsages_display_error('Problem sending kudos.Please notify the system administrator!');
            return;
        }
        echo "<br/><div class='innovage_message'>Kudos have been sent. Click <a href='" . esc_url(home_url('/')) . "'>here</a> to return to your dashboard.</div><br/>";
    }

    innovage_messages_show_form($recipient);
}

/** /
 * Save the submitted message to bp activity
 *
 * @return boolean
 */
function innovage_message_add_activity($recipient) {
    // Check the nonce
    check_admin_referer('post_update', '_wpnonce_post_update');

    // Get activity info
    $content = apply_filters('bp_activity_post_update_content', $_POST['whats-new-select']);
    $current_user = bp_loggedin_user_id();
    $primary_link = bp_core_get_userlink($current_user, false, true);

    if (empty($content) || $content == '') {
        return false;
    }
    if (!innovage_messages_is_valid_message($content, $recipient)) {
        return false;
    }

    // add the activity
    $activity_id = bp_activity_add(array(
        'action' => '', // The activity action - e.g. "Jon Doe posted an update"
        'content' => $content, // Optional: The content of the activity item e.g. "BuddyPress is awesome guys!"
        'component' => buddypress()->activity->id, // The name/ID of the component e.g. groups, profile, mycomponent
        'type' => 'activity_update', // The activity type e.g. activity_update, profile_updated
        'primary_link' => apply_filters('bp_activity_new_update_primary_link', $primary_link), // Optional: The primary URL for this item in RSS feeds (defaults to activity permalink)
        'user_id' => $current_user, // Optional: The user to record the activity for, can be false if this activity is not for a user.
        'item_id' => false, // Optional: The ID of the specific item being recorded, e.g. a blog_id
        'secondary_item_id' => false, // Optional: A second ID used to further filter e.g. a comment_id
        'recorded_time' => bp_core_current_time(), // The GMT time that this activity was recorded
        'hide_sitewide' => false, // Should this be hidden on the sitewide activity stream?
        'is_spam' => false, // Is this activity item to be marked as spam?
    ));

    // Add this update to the "latest update" usermeta so it can be fetched anywhere.
    bp_update_user_meta(bp_loggedin_user_id(), 'bp_latest_update', array(
        'id' => $activity_id,
        'content' => $content
    ));
    return $activity_id;
}

/** /
 * Output html for the add message form
 *
 * @global type $bp
 * @global type $wp
 */
function innovage_messages_show_form($recipient) {
    global $bp, $wp;
    $allowed_messages = innovage_messages_get_messages();

    $recipient_slug = innovage_messages_get_user_slug($recipient->ID);
    ?>
    <form method="post" id="message_activity_form" name="message_activity_form" >
        <div id="whats-new-avatar">
            <a href="<?php echo bp_loggedin_user_domain(); ?>">
                <?php bp_loggedin_user_avatar('width=' . bp_core_avatar_thumb_width() . '&height=' . bp_core_avatar_thumb_height()); ?>
            </a>
        </div>
        <p class="activity-greeting"> <?php
            printf(__("Hi %s. Send a kudos message to %s?", 'buddypress'), bp_core_get_userlink(bp_loggedin_user_id()), bp_core_get_userlink($recipient->ID));
            ?></p>

        <select name="whats-new-select" id="whats-new-select" >
            <?php
            foreach ($allowed_messages as $message) {
                echo '<option value="@' . $recipient_slug . ' ' . $message . '">' . $message . '</option>';
            }
            ?>
        </select> 
        <br/><br/>
        <input type="submit" name="submit" 
               id="innovage-message-submit" 
               value="Send Kudos" />

        <?php wp_nonce_field('post_update', '_wpnonce_post_update'); ?>
    </form>
    <?php
}

// Do not allow commenting on activity
add_filter('bp_activity_can_comment', '__return_false');

// Do not allow favorite-ing on activity
add_filter('bp_activity_can_favorite', '__return_false');

// Disable activity feeds
add_filter('bp_activity_enable_feeds', '__return_false');

/** /
 * Change the Profile -> Activity -> Mentions sub nav sting to say Kudos
 * @global type $bp
 */
function innovage_messages_change_profile_activity_nav() {
    global $bp;

    //remove tabs from group
    if (bp_is_group()) {
        $parent_slug = isset($bp->bp_options_nav[$bp->groups->current_group->slug]) ? $bp->groups->current_group->slug : $bp->groups->slug;

        // hide the home tab with its default activity stream
        $bp->bp_options_nav[$parent_slug]['home'] = false;

        // redirect the group home tab (otherwise defaulting to activity) to the members tab
        // only redirect if the user has access to the members tab
        if (bp_is_group_home() && $bp->bp_options_nav[$parent_slug]['members']["user_has_access"]) {
            bp_core_redirect(bp_get_group_permalink($bp->groups->current_group) . 'members/');
        }
    }

    // If in user profile
    // remove sub-tabs from the activity tab
    if (bp_is_user()) {
        bp_core_remove_subnav_item('activity', 'groups');
        bp_core_remove_subnav_item('activity', 'favorites');
    }

    $bp->bp_options_nav['activity']['mentions']['name'] = 'Kudos';
}

add_action('bp_setup_nav', 'innovage_messages_change_profile_activity_nav', 201);

/** /
 * Display the error messages to screen
 *
 * @param type $error
 */
function innovage_messsages_display_error($error) {
    echo "<br/><span class='innovage_error'>";
    echo 'Error: ' . $error;
    echo "</span>";
}

/** /
 * Returns array of 'allowed' messages
 *
 * @return type
 */
function innovage_messages_get_messages() {
    return array('Congratulations',
        'You are amazing',
        'Well done',
        'Amazing!',
        'Thank you');
}

/** /
 * Only allow the message to be one of those hadcoded in array returned in
 * innovage_messages_get_messages()
 *
 * @param type $message
 * @param type $recipient
 * @return boolean
 */
function innovage_messages_is_valid_message($message, $recipient) {
    $allowed_messages = innovage_messages_get_messages();
    $recipient_slug = innovage_messages_get_user_slug($recipient->ID);
    $message = trim(str_replace('@' . $recipient_slug, '', $message));

    if (in_array($message, $allowed_messages)) {
        return true;
    }
    return false;
}

/** /
 * Small function to handle getting user slug based on userid
 * @param type $user_id
 * @return type
 */
function innovage_messages_get_user_slug($user_id) {
    $slug = bp_core_get_username($user_id);

    if (bp_is_username_compatibility_mode()) {
        $slug = rawurlencode($slug);
    }
    return $slug;
}

/** /
 * Alter the message content in the email notification send when Kudos are saved.
 * Link should take user to their Activity -> Kudos page
 *
 * @param type $message
 * @param type $poster_name
 * @param type $content
 * @param type $message_link
 * @param type $settings_link
 * @return type
 */
function innovage_messages_kudos_notification_filter($message, $poster_name, $content, $message_link, $settings_link) {
    if (bp_is_active('groups') && bp_is_group()) {
        return;
    }
    $user_activity_kudos_link = str_replace('/settings/notifications/', '/activity/mentions/', $settings_link);
    $message = sprintf(__(
                    '%1$s mentioned you in an update:

"%2$s"

To view the message, log in and visit: %3$s

---------------------
', 'buddypress'), $poster_name, $content, $user_activity_kudos_link);

    return $message;
}

add_filter('bp_activity_at_message_notification_message', 'innovage_messages_kudos_notification_filter', 1, 6);

